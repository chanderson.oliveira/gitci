---
title: Hello Docusauro.io
author:  Nakagawa
authorURL: http://twitter.com/ericnakagawa
authorFBID: 661277173
---

This blog post will test file name parsing issues when periods are present.


<!--truncate-->

##Agora sim!

Mauris vestibulum ullamcorper nibh, ut semper purus pulvinar ut. Donec volutpat orci sit amet mauris malesuada, non pulvinar augue aliquam. Vestibulum ultricies at urna ut suscipit. Morbi iaculis, erat at imperdiet semper, ipsum nulla sodales erat, eget tincidunt justo dui quis justo. Pellentesque dictum bibendum diam at aliquet. Sed pulvinar, dolor quis finibus ornare, eros odio facilisis erat, eu rhoncus nunc dui sed ex. Nunc gravida dui massa, sed ornare arcu tincidunt sit amet. Maecenas efficitur sapien neque, a laoreet libero feugiat ut.

##Usar dark.css
```
drwxr-xr-x   8 chandersonoliveira root                 4096 ago 20 15:46 ./
drwxr-xr-x   4 chandersonoliveira chandersonoliveira   4096 ago 20 15:49 ../
drwxr-xr-x   2 chandersonoliveira root                 4096 ago 21 15:23 blog/
drwxr-xr-x   2 chandersonoliveira root                 4096 ago 20 15:28 core/
drwxr-xr-x   2 chandersonoliveira root                 4096 ago 20 15:46 i18n/
drwxr-xr-x 697 chandersonoliveira root                20480 ago 20 15:46 node_modules/
-rw-r--r--   1 chandersonoliveira root                  376 ago 20 15:28 package.json
drwxr-xr-x   3 chandersonoliveira root                 4096 ago 20 15:28 pages/
-rwxr-xr-x   1 chandersonoliveira root                 4065 ago 20 15:28 README.md*
-rwxr-xr-x   1 chandersonoliveira root                  191 ago 21 15:16 sidebars.json*
-rw-r--r--   1 chandersonoliveira root                 3426 ago 21 15:18 siteConfig.js
drwxr-xr-x   4 chandersonoliveira root                 4096 ago 20 15:28 static/
-rw-r--r--   1 chandersonoliveira root               281791 ago 20 15:28 yarn.lock

```